## ReactJs-Hiring-Assignment

**React JS Assignment(Mandatory)**

Create a project in React JS to demonstrate a project management system. The functionality should include the ability to manage projects and tasks. Each Project will have multiple tasks.

---

#### Features

**User Profile**

* Sigin Page
* Signup Page
* Profile Page

**Project**

* List all the Project
* Create a new Project
* Delete a project
* Edit the project status

**Tasks**

* List all the tasks
* Create a new task
* Delete a task
* Edit the task status

---
### Table Column Detail

**Profile**:

- Username
- First Name
- Last Name
- Email
- Address

** Project**:

- Name
- Description
- Creator
- StartDate(The day when the entry is created)
- Enddate

**Task**:

- Name
- Description
- Status(TODO, WIP, ONHOLD, DONE)

## Submission:

Fork this repository to your personal github/bitbucket account. Once you're done with your solution, send the mail with repository details.

## Timeline:

You will get one week to complete the assignment. Please let us know in advance if for whatever reasons you cannot complete it within the said duration but you intent to

## Hygiene:

Explain with comments wherever you are doing something complex
Define your functions/classes/variables with clear naming conventions
